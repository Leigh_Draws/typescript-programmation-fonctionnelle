# TypeScript - Programmation Fonctionnelle

## Initialisation d'un projet TypeScript avec Node

##### Mise en place

- npm init -y --- *initialise le projet node*
- Créer les dossiers *src* (pour le fichiers TypeScript) et *dist* (pour le fichier JavaScript qui se créera automatiquement quand on compilera le TS)
- npm i --save-dev typescript @types/node --- *installe TypeScript*
- Créer un fichier tsconfig.json avec la configuration de TypeScript souhaitée
- Rajouter la ligne dans package.json :
    "script" : {
        "build" : "tsc"
    }
- Maintenant quand on écrit "npm run build" le TS est compilé en JS

##### TS-Node et Nodemon 

- npm i --save-dev ts-node 
    --- *Pour lancer l'application sans passer par la compilation*
- npm i --save-dev nodemon
    --- *Pour relancer le serveur quand le fichier source change*
- Rajouter la ligne dans package.json :
    "scripts": {
    "dev": "nodemon"
    }
- Créer un fichier de configuration pour Nodemon (nodemon.json)

##### Prettier et ESLint

- npm i --save-dev eslint @typescript-eslint/eslint-plugin @typescript-eslint/parser
- Créer un fichier de configuration .eslintrc

- npm i --save-dev prettier eslint-config-prettier eslint-plugin-prettier
- Créer un fichier de configuration .prettierrc

##### Jest : Framework pour les tests

- npm i --save-dev jest @types/jest ts-jest eslint-plugin-jest 
- Créer un fichier de configuration jest.config.ts

## 1. Convertisseur de devises

→ Création d'une fonction *convertCurrency* qui prend en paramètres un nombre (le montant), une chaîne de caractère (pour la devise de départ) et une chaîne de caractère (pour la devise de sortie)
→ La fonction renvoie une chaîne de caractète : Le montant + la devise

## 2. Calculateur de frais de livraison

→ Création d'une fonction *shippingFeeCalculator* qui prend en paramètres le poids (en kg), les dimensions du colis (hauteur, largeur, longueur en cm) et le pays de destination
→ La fonction renvoie une chaîne de caractères : Le montant + la devise

## 3. Calcul des frais de douanes

→ Création d'une fonction *customsFeeCalculator* qui prend en paramètres la valeur du colis (un nombre), et le pays de destination (une chaîne de caractères)
→ La fonction renvoie une chaîne de caractères : Le montant + la devise

## Intéraction avec l'utilisateur en lignes de commandes

##### Installation de Prompt sync

→ Intéraction en ligne de commande pour lancer les fonctions avec des valeurs données par l'utilisateur
- npm install prompt-sync
- Ne fonctionne pas bien avec Nodemon, compiler puis executer le JavaScript