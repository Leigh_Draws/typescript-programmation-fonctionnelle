
// Les paramètres en entrée doivent être un nombre, une chaîne de caractère (pour la devise de départ) et une chaîne de caractère (pour la devise de sortie)
// La fonction renvoie un nombre (le montant converti)
function convertCurrency(amount: number, startingCurrency: string, destinationCurrency: string): number | string {
    // Variable qui va stocker la valeur en chaîne de caractère pour pouvoir l'afficher avec sa devise
    let convertedAmount: string = '';
    // Les cas où la devise de départ est en EUR
    if (startingCurrency === 'EUR') {
        if (destinationCurrency === 'CAD') {
            amount = amount * 1.5;
            convertedAmount = `${amount} CAD`;
        } else if (destinationCurrency === 'JPY') {
            amount = amount * 130;
            convertedAmount = `${amount} JPY`
        } else if (destinationCurrency === 'EUR') {
            amount = amount;
            convertedAmount = `${amount} EUR`
        }
        // Les cas où la devise de départ est en CAD
    } else if (startingCurrency === 'CAD') {
        if (destinationCurrency === 'EUR') {
            amount = amount * 0.67;
            convertedAmount = `${amount} EUR`;
        } else if (destinationCurrency === 'JPY') {
            amount = amount * 87;
            convertedAmount = `${amount} JPY`
        } else if (startingCurrency === 'CAD') {
            amount = amount;
            convertedAmount = `${amount} JPY`
        }
        // Les cas où la devise de départ est en JPY
    } else if (startingCurrency === 'JPY') {
        if (destinationCurrency === 'CAD') {
            amount = amount * 0.0115;
            convertedAmount = `${amount} CAD`;
        } else if (destinationCurrency === 'EUR') {
            amount = amount * 0.0077;
            convertedAmount = `${amount} EUR`
        } else if (startingCurrency === 'JPY') {
            amount = amount;
            convertedAmount = `${amount} JPY`
        }
    }
    // Renvoie le montant en dehors des if
    return convertedAmount
}

// Fonction pour calculer les frais de livraison
// Le poids et les dimensions sont des nombres et le pays est une chaîne de caractère
// La fonction renvoie une chaîne de caractères
function shippingFeeCalculator(weight: number, length: number, width: number, height: number, country: string): string {
    // Variable qui stocke le montant des frais
    let fee = 0;
    // Variable qui va stocker la valeur en chaîne de caractère pour pouvoir l'afficher avec sa devise
    let convertedFee = '';
    // Variable qui stocke les valeurs ajoutées des dimensions
    let totalDimensions = length + width + height;
    // Le cas où le colis ferait plus de 150 en dimension
    if (totalDimensions > 150) {
        if (country === 'France') {
            fee += 5;
        } else if (country === 'Canada') {
            fee += 7.5;
        } else if (country === 'Japon') {
            fee += 500;
        }
        //Le cas où le poids du colis serait inférieur ou égal à 1kg
    } if (weight <= 1) {
        if (country === 'France') {
            fee += 10;
            convertedFee = `${fee} EUR`;
        } else if (country === 'Canada') {
            fee += 15;
            convertedFee = `${fee} CAD`;
        } else if (country === 'Japon') {
            fee += 1000;
            convertedFee = `${fee} JPY`;
        }
        //Le cas où le poids du colis serait entre 1kg et 3kg
    } else if (weight > 1 && weight <= 3) {
        if (country === 'France') {
            fee += 20;
            convertedFee = `${fee} EUR`;
        } else if (country === 'Canada') {
            fee += 30;
            convertedFee = `${fee} CAD`;
        } else if (country === 'Japon') {
            fee += 2000;
            convertedFee = `${fee} JPY`;
        }
        //Le cas où le poids du colis serait supérieur à 3kg
    } else if (weight > 3) {
        if (country === 'France') {
            fee += 30;
            convertedFee = `${fee} EUR`;
        } else if (country === 'Canada') {
            fee += 45;
            convertedFee = `${fee} CAD`;
        } else if (country === 'Japon') {
            fee += 3000;
            convertedFee = `${fee} JPY`;
        }
    }
    // Renvoie une chaîne de caractères avec les frais additionnés et la devise du pays 
    return convertedFee

}


// Fonction qui calcule les frais de douane
// Prend en paramètre la valeur de l'objet, un nombre et le pays de destination, une chaîne de caractère
// Renvoie une chaîne de caractères
function customsFeesCalculator(value: number, shippedCountry: string): string {
    let fee = 0;
    let result = ''
    // Quand le colis est envoyé au Canada
    if (shippedCountry === 'Canada') {
        // Vérifie s'il coûte plus de 20
        // Si oui, renvoie le montant de la taxe (seulement la taxe pas le montant total)
        if (value > 20) {
            fee = (value / 100) * 15;
            result = `${fee} CAD`;
        } else {
            result = `${fee} CAD`;
        }
        // Quand il est envoyé au Japon
    } else if (shippedCountry === 'Japon') {
        // Vérifie s'il coûte plus de 5000
        if (value > 5000) {
            fee = (value / 100) * 10;
            result = `${fee} JPY`;
        } else {
            result = `${fee} JPY`;
        }
        // S'il est envoyé en France
    } else if (shippedCountry === 'France') {
        // Pas de taxe donc renvoie 0, la valeur de "fee" de base
        result = `${fee} EUR`;
    }
    return result
}

const prompt = require('prompt-sync')({ sigint: true });

// Questions posées dans le terminal pour lancer la fonction de conversion
const userAmount = prompt('Quel montant voulez-vous convertir ? ');
const amountNumber = parseFloat(userAmount);
if (isNaN(amountNumber)) {
    console.log("Merci de donner un nombre");
} else {
    const userInCurrency = prompt('Depuis quelle devise devons-nous convertir ? (EUR, CAD ou JPY) ');
    const userOutCurrency = prompt('Dans quelle devise devons-nous convertir le montant ? (EUR, CAD ou JPY) ');
    console.log(`Conversion de ${userAmount} ${userInCurrency} en ${userOutCurrency} : `);

    const result = convertCurrency(amountNumber, userInCurrency, userOutCurrency);
    console.log(result);
}

// Questions posées dans le terminal pour lancer la fonction de calcul des frais de livraison
const userWeight = prompt('Quel est le poid du colis ? ');
const userLength = prompt('Longueur du colis ? ');
const userWidth = prompt('Largeur du colis ? ');
const userHeight = prompt('Hauteur du colis ? ');
const userCountry = prompt('Pays de destination ? ');
const resultat = shippingFeeCalculator(userWeight, userLength, userWidth, userHeight, userCountry);
console.log(resultat)

// Questions posées dans le terminal pour lancer la fonction de calcul des frais de douane
const userPackageValue = prompt('Valeur du colis : ');
const userCountryDestination = prompt('Pays de destination ? ');
const answer = customsFeesCalculator(userPackageValue, userCountryDestination)
console.log(answer)



